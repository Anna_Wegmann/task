/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DemandFnc.h
 * Author: anna
 *
 * Created on October 3, 2016, 5:00 PM
 */

#ifndef DEMANDFNC_H
#define DEMANDFNC_H
#include "UsedTypes.h"

class DemandFnc {
public:
        DemandFnc(int fncType, FPType alpha, FPType beta, FPType orgDemand);
        virtual ~DemandFnc();
        FPType evaluate(FPType cost);
        void setUpperBound(FPType upperBound);
private:
        int fncType_;
        FPType alpha_;
        FPType beta_;
        FPType orgDemand_;
        FPType upperDemBound_;

};

#endif /* DEMANDFNC_H */

