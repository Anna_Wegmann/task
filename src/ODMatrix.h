#ifndef OD_MATRIX
#define OD_MATRIX

#include "UsedTypes.h"
#include "NetData.h"


#include <string>
#include <tr1/unordered_map>
#include <vector>

class Origin;

typedef std::vector<Origin*>::iterator OriginIterator;
typedef std::vector<Origin*>::const_iterator ConstOriginIterator;

/** \brief This class implements origin-destination matrix.
*/
class ODMatrix {
	public:
                /**
                 * create ODMatrix from netData 
                 * @param netData - has been read in by NetParser..
                 */
                ODMatrix(NetData *netData, bool carNetwork=true);
                
		~ODMatrix();
		
		/** This method is used for creation of the matrix.
			Adds Origin object  to O-D matrix.
		*/
		void addOrigin(Origin *origin);
		
		/** @return iterator pointing to the beginning of he container.
		*/
		OriginIterator begin();

		/** @return end of the container.
		*/
		OriginIterator end();

		ConstOriginIterator begin() const;
		ConstOriginIterator end() const;
		
		/** This method creates O-D indexes for all O-D pairs and fills the data structure
			used to support getDemandByIndex() method. 
			\note It MUST be called once when add origins have been added to the class.
		*/
		void setIndexes();
		
		/** Prints O-D matrix on the screen.
		*/
		void print();
		
		/** @return total number of O-D pairs.
		*/
		int getNbODPairs() const;
		/** @return total number of origins.
		*/
		int getNbOrigins() const;
		
		/** This method returns demand of node \b destIndex coming from origin \b originIndex.
			\note Complexity in average case: constant; worst case: linear in container size.
			For details see http://www.cplusplus.com/reference/unordered_map/unordered_map/find/.
		*/
		FPType getDemandByIndex(int originIndex, int destIndex) const;

		/** @return pointer to the Origin object given origin index.
		*/
		Origin* getOriginByIndex(int index) const;
                
                /** 
                 Called for the elastic Demand Excess Demand Implementation. MaxDemand is set to double the expected Demand.
                 */
                
                void doubleDemand();
                
                /**
                 * checks if origin has no destinations and removes origin from odMatrix if that is the case
                 * might throw an error
                 * @param originIndex
                 */
                bool checkDemandAndRemoveOrigin(int originIndex);
                
                /**
                 * removes the ODPair matching originIndex, destIndex
                 * and removes demand form demandByIndex with matching key to originIndex,destIndex
                 * purpose: should only be called for ODPair with demand=0
                 * might throw an error
                 * @param originIndex
                 * @param destIndex
                 */
                void removeDest(int originIndex, int destIndex);
		
	private:
	
		const int nbNodes_; 
		int nbODPairs_;
		std::vector<Origin*> odMatrix_; /**< internal representation of O-D matrix */
		std::tr1::unordered_map<std::string, FPType> demandByIndex_; /**< special structure that allows to */
																	/**< get demand of node given origin and destination */
																	/**< indexes */	
		inline std::string createKey(int originIndex, int destIndex) const;	
};

#endif
