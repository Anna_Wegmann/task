#include "DefaultNetParser.h"
#include "UtilsString.h"
#include "Error.h"
#include "BprFnc.h"
#include "InvExDemFnc.h"
#include "FileReader.h"
#include "StarNetwork.h"
#include "StarNode.h"
#include "StarLink.h"
#include "NetData.h"

#include <sstream>
#include <stdlib.h>

/** Internal utility structure.
*/ 
struct ParamsLine{
	int origin;
	int dest;
	FPType capacity;
	FPType length;
	FPType freeFlowTime;
	FPType b;
	FPType power;
	FPType speed;
	FPType toll;
	int type;
        int eLane;
};

DefaultNetParser::DefaultNetParser(const std::string& fileWithNetwork) : 
				fileWithNetwork_(fileWithNetwork) {

};

DefaultNetParser::~DefaultNetParser(){

};

NetData* DefaultNetParser::parseNetwork(bool eLanes){
	FileReader readFile(fileWithNetwork_);
		
	std::string netName("");
	Utils::extractName(fileWithNetwork_.c_str(), netName);
	
	DataFromHeader data = parseDataFromHeader(readFile);
        
        NetData* netData = new NetData(netName, data.nbNodes, data.nbLinks, data.firstNode-1, eLanes);
        
	allocateTolls(data.nbLinks);

	char semic;
	ParamsLine params;
        params.eLane = 0;
        
	std::string line("");

	while (readFile.isGood()) { 
		line = readFile.getNextLine(); 
		line = Utils::skipOneLineComment("~", line);
		
		if (!Utils::deleteWhiteSpaces(line).empty()){
    		std::stringstream strLine(line);
			strLine >> params.origin >> params.dest >> params.capacity >> params.length >> 
						params.freeFlowTime >> params.b >> params.power >> params.speed >> 
						params.toll >> params.type;
                        if (eLanes) { //CHANGED: should be no problem since getNextLine() just ignores last characters and gets the next Line in file
                                strLine >> params.eLane;
                        }
                        strLine >> semic; //necessary?
                        
                        netData->addLinkData(params.origin, new LinkData(params.dest, params.capacity, params.length, params.freeFlowTime, params.b, params.power, params.speed, params.toll, params.type, params.eLane==1));
		}                
	}
	return netData;	 
};

DataFromHeader DefaultNetParser::parseDataFromHeader(FileReader& readFile) {
	std::string line("");
	std::string field("");
	std::string value("");
	DataFromHeader data;
	data.nbNodes = 0;
	data.nbLinks = 0;
	data.firstNode = 0;
	while (line.find("<END OF METADATA>") == std::string::npos) {
		line = readFile.getNextLine();
		line = Utils::skipOneLineComment("~", line);
		
		if (!Utils::deleteWhiteSpaces(line).empty()){
			field = Utils::getSubString("<", ">", line);
			value = line.substr(line.find(">") + 1);
			if (field == "NUMBER OF NODES"){
				data.nbNodes = atoi(value.c_str());
			} else if (field == "NUMBER OF LINKS") { 
				data.nbLinks = atoi(value.c_str());
			} else if (field == "FIRST THRU NODE") {
				data.firstNode = atoi(value.c_str());
			}
		}
		if (!readFile.isGood()) {
			std::string message = "<END OF METADATA> is missing in file: ";
			message.append(fileWithNetwork_);
			Error er(message);
			throw er;
		}
	}
	Utils::checkEmptyValue(data.nbNodes);
	Utils::checkEmptyValue(data.nbLinks);
	Utils::checkEmptyValue(data.firstNode); 
	return data;	
};