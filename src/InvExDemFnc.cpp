/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ExcessDemandFnc.cpp
 * Author: anna
 * 
 * Created on September 23, 2016, 11:00 AM
 */

#include "InvExDemFnc.h"
#include "Error.h"

#include <cassert>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <limits>

const int DIVIDER = 5;
const int initialFlow = 200;

InvExDemFnc::InvExDemFnc(int fncType, FPType alpha, FPType beta, FPType orgDemand) : fncType_(fncType), alpha_(alpha), beta_(beta), orgDemand_(orgDemand), upperDemandBound_(0) {
        
}

InvExDemFnc::~InvExDemFnc() {
};


// Excess Demand Function: \beta e^{-\alpha x}

// Excess Demand Function: \beta e^{-\alpha x}
void InvExDemFnc::checkExcessDemand(FPType excessDemand) const{
        if (excessDemand<0) {
                std::cout << "upperDemandBound: " << upperDemandBound_ << " link flow: " << upperDemandBound_-excessDemand << std::endl;
                std::cout << "excessDemand: " << excessDemand << std::endl;
                throw Error("Link flow on forward generating link is higher than the upper demand bound.");
        }
}

// Excess Demand: Inverse Demand (maxDemand-Flow)
FPType InvExDemFnc::evaluate(FPType linkFlow) const {
        if (upperDemandBound_==0) {
                return 999999; // upperDemandBound initialized as 0... returns high value so shortest path algorithm in ObjectManager won't use fwdGenPath.. (maybe separate network??)
        } 
        FPType excessDemand = upperDemandBound_-linkFlow;
        checkExcessDemand(excessDemand);
        
        if (fncType_==0) { /* ********* exponential function demand case beta_*orgDemand*e^(-alpha_*x) ********/
                if (orgDemand_==0||excessDemand==0) {
                        return 999999;
                } else {
                        return (log(beta_*orgDemand_)-log(excessDemand))/alpha_;
                }
        } else if (fncType_==1) { /* ******************++++++++++++++++ 2/3 Node example ****************/
                return (beta_-excessDemand)/alpha_;
                //return 1 + linkFlow;
        } else {
                throw Error("no InvExDemFnc with this type exists..");
        }
};

FPType InvExDemFnc::evaluateDerivative(FPType linkFlow) const {
        if (upperDemandBound_==0) { return 999999;}
        FPType excessDemand = upperDemandBound_-linkFlow;
        checkExcessDemand(excessDemand);
       
        if (fncType_==0) { /* ********* exponential function demand case ********/
                if (orgDemand_==0 || 1/(alpha_*(excessDemand))>999999) {
                        //return std::numeric_limits<FPType>::infinity(); //999999;
                        return 999999;
                } else {
                        return 1/(alpha_*(excessDemand));
                }                
        } else if (fncType_==1) {/* *********** 2/3 Node example ************** */
                return 1/alpha_;
        } else {
                throw Error("no InvExDemFnc with this type exists..");
        }
       


};


void InvExDemFnc::print() const {
    std::cout << "invExDemFnc " << std::endl;
    std::cout << "alpha = " << alpha_ << " beta= " << beta_ <<std::endl;
};

FPType InvExDemFnc::evaluateSecondDerivative(FPType linkFlow) const {
        if (upperDemandBound_==0) { return 999999;}
        FPType excessDemand = upperDemandBound_-linkFlow;    
        checkExcessDemand(excessDemand);

        if (fncType_==0) {/* ********* exponential function demand case ********/
                if (orgDemand_==0 || 1/(alpha_*(excessDemand)*(excessDemand))>999999) {
                        //return std::numeric_limits<FPType>::infinity(); //999999;
                        return 999999;
                } else {
                        return 1/(alpha_*(excessDemand)*(excessDemand));
                }                
        } else if (fncType_==1) {  /*************** linear & constant demand case *******/
                return 0;
        } else {
               throw Error("no InvExDemFnc with this type exists..");
        }
};

FPType InvExDemFnc::evaluateTimeAndDerivative(FPType linkFlow, FPType& der) { // der: Ref. to value der and return value fnc
        der = evaluateDerivative(linkFlow);
        return evaluate(linkFlow);
};

FncType InvExDemFnc::getType() {
    return INVEXDEMFNC;
};

bool InvExDemFnc::maxDemandSet() const {
    if (beta_<0) {
        throw Error("Max Demand has not been set yet.");
    }
    return true;
};

void InvExDemFnc::setUpperDemandBound(FPType upperBound) {
        upperDemandBound_=upperBound;
};

FPType InvExDemFnc::getUpperDemandBound(){
        return upperDemandBound_;
};