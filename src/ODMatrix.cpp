#include "ODMatrix.h"
#include "Origin.h"
#include "PairOD.h"
#include "Error.h"

#include <cassert>
#include <iostream>
#include <sstream>

ODMatrix::ODMatrix(NetData* netData, bool carNetwork) : nbNodes_(netData->getNbNodes()), nbODPairs_(0), odMatrix_(netData->getNbOrigins()) {
        assert(nbNodes_ > 0);
        NodeData *nodeData;
        for (int i=0; i<nbNodes_; i++) {
                nodeData = netData->getNodeData(i);
                if (nodeData->getFwdGenLinkDataSize()!=0) { // node is Origin...
                        Origin* origin = new Origin(i);
                        for (int j=0; j<nodeData->getFwdGenLinkDataSize(); j++) {
                                FwdGenLinkData* fwdGenLinkData = nodeData->getFwdGenLinkData(j);
                                FPType demand = fwdGenLinkData->getDemand();
                                if (netData->isENetwork() && carNetwork) {
                                        demand = 0.8*demand;
                                } else if(netData->isENetwork()) {
                                        demand = 0.2*demand;
                                }
                                origin->addDestination(new PairOD(netData->getIndex(fwdGenLinkData->getDestID()), demand)); //TODO index or ID???                                
                        }
                        //std::cout << "adding origin " << i << std::endl;
                        addOrigin(origin);
                }
        }
        setIndexes();
}

ODMatrix::~ODMatrix(){
	for (std::vector<Origin*>::iterator it = odMatrix_.begin(); it != odMatrix_.end(); ++it){
		delete *it;
	}
};

int ODMatrix::getNbOrigins() const{
	return odMatrix_.size(); 
};

void ODMatrix::addOrigin(Origin *origin){ // all origins have to be counted one after another... 
	assert(getNbOrigins() <= nbNodes_ && origin->getIndex() >= 0);
	odMatrix_[origin->getIndex()] = origin;
};

OriginIterator ODMatrix::begin(){
	return odMatrix_.begin();
};


OriginIterator ODMatrix::end(){
	return odMatrix_.end();
};

ConstOriginIterator ODMatrix::begin() const {
	return odMatrix_.begin();
};

ConstOriginIterator ODMatrix::end() const {
	return odMatrix_.end();
};

void ODMatrix::print(){
	std::cout << "nb pairs = " << nbODPairs_ << std::endl;
	for (OriginIterator it = begin(); it != end(); ++it){
		Origin* origin = *it; 
		std::cout << "Origin: " << origin->getIndex() << std::endl;
		for (PairODIterator jt = origin->begin(); jt != origin->end(); ++jt) {
			PairOD* dest = *jt;
			dest->print();
		}
	};
};

void ODMatrix::setIndexes(){
        demandByIndex_.clear();
        assert(demandByIndex_.empty());
	int count = 0;
	int originIndex = -1;
	//
	for (OriginIterator it = begin(); it != end(); ++it){
		Origin* origin = *it; 
		originIndex = origin->getIndex(); 
		for (PairODIterator jt = origin->begin(); jt != origin->end(); ++jt) {
			PairOD* dest = *jt;
			dest->setODIndex(count);
			demandByIndex_.insert(std::make_pair<std::string, FPType>(
					createKey(originIndex, dest->getIndex()), 
							  dest->getDemand()));
			++count;
		}
	}
	nbODPairs_ = count;
	std::cout << "nbODPairs = " << nbODPairs_ << std::endl;	
};

FPType ODMatrix::getDemandByIndex(int originIndex, int destIndex) const{
	std::tr1::unordered_map<std::string, FPType>::const_iterator got = demandByIndex_.find(createKey(originIndex, destIndex));
	if (got == demandByIndex_.end()) return 0.0;
	return got->second; 
};

int ODMatrix::getNbODPairs() const{
	return nbODPairs_;
};

std::string ODMatrix::createKey(int originIndex, int destIndex) const{
	std::stringstream key;
	key << originIndex << "_" << destIndex;
	return key.str();
};

Origin* ODMatrix::getOriginByIndex(int index) const {
	//assert(index >= 0 && index < getNbOrigins());
	assert(index>=0);
        if (index < getNbOrigins()) {
                return odMatrix_[index];
        } else {
                return NULL;
        }
        
};

bool ODMatrix::checkDemandAndRemoveOrigin(int originIndex) {
        for (OriginIterator it=begin();it != end(); ++it) {
                Origin* origin = *it;
                if (origin->getIndex()==originIndex) {
                        if (origin->getNbDest()==0) {
                                std::cout << "original size od-Matrix: " << getNbOrigins();
                                odMatrix_.erase(it);
                                std::cout << " an origin was just removed... new size of odMatrix" << getNbOrigins() << std::endl;
                                return true;
                        }
                        return false;
                }
        }
        throw Error("Error in ODMatrix::checkDemandAndRemoveOrigin.... originIndex could not be found...");
};

void ODMatrix::removeDest(int originIndex, int destIndex) {
        /*** erase demand from demandByIndex map... *****/
        std::string key = createKey(originIndex, destIndex);
        std::tr1::unordered_map<std::string, FPType>::const_iterator got = demandByIndex_.find(key);
        demandByIndex_.erase(got);
        
        /*** erase demand from origin vector ****/
        Origin* origin = NULL;
        for (OriginIterator it=begin(); it!=end(); ++it) {
                origin = *it;
                if (origin->getIndex()==originIndex) {
                        for (PairODIterator jt=origin->begin(); jt!=origin->end(); ++jt) {
                                if ((*jt)->getIndex()==destIndex) {
                                        origin->remove(jt);
                                        return;
                                }
                        }
                }
        }
        throw Error("in ODMatrix::removeDest - originIndex/destIndex pair  is not in odMatrix...");
};