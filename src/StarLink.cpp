#include "StarLink.h"
#include "Error.h"
#include "InvExDemFnc.h"

#include <sstream>
#include <cmath>

StarLink::StarLink(int nodeFromID, int nodeToID, LinkFnc *fnc, bool isELane) : index_(-1),  
				  nodeFromID_(nodeFromID), nodeToID_(nodeToID),
				  nodeFromIndex_(-1), nodeToIndex_(-1), flow_(0.0), baseLoad_(0.0), fnc_(fnc), der_(0.0), eLane_(isELane) { 
        fwdGenLink_=false;
        demandFnc_ = NULL;
        updateTime();
};

StarLink::StarLink(int nodeFromID, int nodeToID, LinkFnc *costFnc, DemandFnc *demandFnc) : index_(-1),  
				  nodeFromID_(nodeFromID), nodeToID_(nodeToID),
				  nodeFromIndex_(-1), nodeToIndex_(-1), flow_(0.0), baseLoad_(0.0), fnc_(costFnc), der_(0.0), eLane_(false) {
        fwdGenLink_ = true;
        demandFnc_ = demandFnc;
        updateTime();
}


StarLink::~StarLink(){
	delete fnc_;
        delete demandFnc_;
	fnc_ = NULL;
        demandFnc_=NULL;
};

int StarLink::getNodeFrom() const{    
    return nodeFromID_;
};

int StarLink::getNodeTo() const{
	return nodeToID_;
};

LinkFnc* StarLink::getLinkFnc() const{
	return fnc_;
};

FPType StarLink::getFlow() const{
	return flow_;
};

void StarLink::setFlow(FPType flow){
        if (fwdGenLink_ && flow>upperDemBound_) {
                throw Error("Attempt to set flow higher than upperDemBound_ for this forward generating link...");
        }
        /*std::cout << "setting flow " << flow_ << " to " << flow << " on link " << toString() << std::endl;*/
	flow_ = flow;
        //DEBUG
        /*updateTime();
        std::cout << "time on link is now " << getTime() << std::endl;*/
};

int StarLink::getIndex() const{
	return index_;
};

void StarLink::setIndex(int index){
	index_ = index;
};

int StarLink::getNodeFromIndex() const{
	return nodeFromIndex_;
};

int StarLink::getNodeToIndex() const{
	return nodeToIndex_;
};

FPType StarLink::getTime() const{
	return time_;
};
		
void StarLink::setNodeToIndex(int index){
	nodeToIndex_ = index;
};

void StarLink::setNodeFromIndex(int index){
	nodeFromIndex_ = index;
};

void StarLink::addFlow(FPType flow){
        /* std::cout << "adding flow " << flow << " to link " << toString() << std::endl; */
	flow_ += flow;
        //DEBUG...
        /* updateTime();
        std::cout << "time on link is now " << getTime() << std::endl; */
        if (fwdGenLink_ && flow_>upperDemBound_) {
                std::cout<<" tried to set flow higher than upperDemandBound by adding flow " << flow << "new flow would have been " << flow_ << " with upper Bound " << upperDemBound_  << std::endl;
                if (std::abs(upperDemBound_-flow_)>exp(-10)){
                        throw Error("größere Abweichung...");
                }
                throw Error("flow higher than demand...");
                flow_=upperDemBound_;
                //throw Error("Attempt to set flow higher than upperDemBound_ for this forward generating link...");
        }
};

void StarLink::updateTime(){
        if (fwdGenLink_ && baseLoad_!=0) {
                throw Error("Baseload for foward generating link should always be 0!");
        }
	time_ = fnc_->evaluateTimeAndDerivative(flow_+baseLoad_, der_); //CHANGED: add baseload
        if (time_<0) {
            std::cout << "for fwd gen Link: " << fwdGenLink_ << " from " << nodeFromIndex_ << " to "
                    << nodeToIndex_ << "time set to: " << time_ <<std::endl;
            //throw Error("Attempt to set cost of travel on link to below 0...");
        }
};

void StarLink::setFlowToZero(){
	flow_ = 0.0;
};

std::string StarLink::toString() const{
	std::stringstream ss;
	ss << "Link: " << index_ << " [" << nodeFromIndex_ << ", " << nodeToIndex_ << "]" 
		<<	" [" << nodeFromID_ << ", " << nodeToID_ << "]";
	return ss.str();
};

FPType StarLink::getDerivative() const{
	return der_;
};

StarLink* StarLink::getForwardLink() {
	return this;
};



DemandFnc* StarLink::getDemandFnc() {
        return demandFnc_;
};

void StarLink::setUpperBound(FPType upperBound) {
        if (!fwdGenLink_) {
                throw Error("Attempt to call setUpperBound() on not forward generating link...");
        }
        upperDemBound_=upperBound;
        DemandFnc * demFnc = dynamic_cast<DemandFnc*>(demandFnc_);
        demFnc->setUpperBound(upperBound);
        InvExDemFnc* invFnc = dynamic_cast<InvExDemFnc*>(fnc_);
        invFnc->setUpperDemandBound(upperBound);
        updateTime();
        if (upperBound != invFnc->getUpperDemandBound()) {
                throw Error("Something went wrong while setting the upper demand Bound");
        }
};