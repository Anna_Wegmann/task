/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DefaultDemandParser.h
 * Author: anna
 *
 * Created on October 18, 2016, 3:05 PM
 */

#ifndef DEFAULTDEMANDPARSER_H
#define DEFAULTDEMANDPARSER_H

#include "NetData.h"
class FileReader;
/** Internal utility structure.
*/


/** \brief Implements parser for vector with pointer to demand functions from text file.
*/
class DefaultDemandParser {
        public:
            /** @param fileWithNetwork path to file with network.
            */
                DefaultDemandParser(const std::string& fileWithNetwork);
                virtual ~DefaultDemandParser();
                
                virtual void parseDemand(NetData* netData);
        protected:
                const std::string fileWithNetwork_;
    

};


#endif /* DEFAULTDEMANDPARSER_H */

