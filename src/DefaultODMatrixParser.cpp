#include "DefaultODMatrixParser.h"
#include "Error.h"
#include "FileReader.h"
#include "UtilsString.h"
#include "Origin.h"
#include "PairOD.h"
#include "ODMatrix.h"
#include "StarNetwork.h"
#include "StarLink.h"
#include "InvExDemFnc.h"

#include <cassert>
#include <stdlib.h>

DefaultODMatrixParser::DefaultODMatrixParser(const std::string& fileWithODMatrix) : 
		fileWithODMatrix_(fileWithODMatrix) {

};

DefaultODMatrixParser::~DefaultODMatrixParser(){

};

void DefaultODMatrixParser::parseODMatrix(NetData* netData){
	FileReader readFile(fileWithODMatrix_);
	// skip metadata
	std::string line = "";
	while (line.find("<END OF METADATA>") == std::string::npos) {
		line = readFile.getNextLine(); 
		
		if (!readFile.isGood()){ 
			std::string message = "<END OF METADATA> is missing in file: ";
			message.append(fileWithODMatrix_);
			Error er(message);
			throw er;
		}
	}
        
        
	size_t found;
	int originID = -1;
	int destID = -1;
	FPType demand = 0.0;
        
	std::string newLine = "";
	while (readFile.isGood()) { 
		line = readFile.getNextLine();
		line = Utils::skipOneLineComment("~", line);
		if (!Utils::deleteWhiteSpaces(line).empty()){
			// check if it is a new origin
			found = line.find("Origin");
			if (found != std::string::npos) {
				originID = atoi((line.substr(found + 7)).c_str());
			}
			// get all destinations from current line
			size_t pos = extractDestination(0, line, destID, demand);
			
			while (pos != std::string::npos){
				if ((destID != originID) && (demand != 0.0)) {
                                        netData->updateDemand(originID, destID, demand);
                                        
				}
				pos = extractDestination(pos, line, destID, demand);
			}
		}
	}
};

size_t DefaultODMatrixParser::extractDestination(size_t posFrom, const std::string& line, 
			int &destID, FPType &demand) const{
	size_t pos1 = line.find(":", posFrom);
	if (pos1 != std::string::npos) {
		size_t pos2 = line.find(";", pos1);
		if (pos2 != std::string::npos) {
			destID = atoi((line.substr(posFrom, pos1 - posFrom - 1)).c_str());
			demand = atof((line.substr(pos1 + 1, pos2 - pos1 - 1)).c_str());
			return pos2 + 1;
		}
		return std::string::npos;
	}
	return std::string::npos;
};