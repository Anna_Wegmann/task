#include "LatexNetOutput.h"
#include "FileWriter.h"
#include "StarNetwork.h"
#include "StarNode.h"
#include "StarLink.h"

#include <sstream>
#include <math.h>
#include <iostream>

#define COORD_TOL 1e-2

LatexNetOutput::LatexNetOutput(StarNetwork* net) : NetOutput(net),  eLanes_(false), carNet_(NULL) {
	setPlotInfoToDefault();
};

LatexNetOutput::LatexNetOutput(StarNetwork* eCarNet, StarNetwork* carNet) : NetOutput(eCarNet), eLanes_(true), carNet_(carNet) {
        setPlotInfoToDefault();
}


LatexNetOutput::LatexNetOutput(StarNetwork* net, const std::string& logFileForMissingIDs) : 
			NetOutput(net, logFileForMissingIDs) {
	setPlotInfoToDefault();
};

LatexNetOutput::~LatexNetOutput(){

};

void LatexNetOutput::setPlotInfoToDefault(){
	snInfo_.side1 = "south";
	snInfo_.side2 = "north";
	snInfo_.shift = "xshift";

	weInfo_.side1 = "west";
	weInfo_.side2 = "east";
	weInfo_.shift = "yshift";
};


void LatexNetOutput::printNet(StarNetwork* net, FileWriter& outputFile,const std::string& fileWithNodes, FPType scale) {
	std::stringstream ss;
	ss << " \\begin{tikzpicture}[scale=" << scale << "] \n";
	outputFile.writeLine(ss.str());
        nextOrientation_="north";
        textPos_="above";
        shift_ = "(\\bx,\\ay+100)";
	
	int nbNodes = net->getNbNodes();
	std::vector<FPType> xCoord(nbNodes, 0);
	std::vector<FPType> yCoord(nbNodes, 0);
	
	std::vector<int> nodeID(nbNodes, 0);
	
	readCoord(fileWithNodes, xCoord, yCoord, nodeID); // xCoord, yCoord is vector

	addTikzNodes(outputFile, xCoord, yCoord, nodeID);

	FlowInfo flowInfo = getFlowInfo(net);
	std::string lastLinkKind = "";
        int lastTail = -1;
        bool sameLink = false;
        
	for (StarNode* node = net->beginNode(); node != NULL; node = net->getNextNode()){
		for (StarLink* link = net->beginLink(); link != NULL; link = net->getNextLink()) {
			
			if (plotLink(link) && (!eLanes_ || net->isECarNetwork() || !link->isELane())) {
				int tail = node->getIndex();
				int head = link->getNodeToIndex();
				std::string lineOptions = generateLineOptions(flowInfo, link);
				
				std::string oneLine("");
				setPlotInfoToDefault();
				if (link->isFwdGenLink()) {
                                        oneLine = drawFwdGenLink(link, lineOptions);
                                        lastLinkKind="fwd";
                                } else {
                                        /*if (tail==lastTail && lastLinkKind=="horizontal" &&  net->isECarNetwork() && link->isELane()) {
                                              sameLink = true;
                                              nextOrientation_="south";
                                              shift_ = "(\\bx,\\ay+100)";
                                              textPos_ = "above";
                                        } else*/ if (tail==lastTail && (( lastLinkKind=="round" && net->isECarNetwork() && link->isELane()) ||
                                                (lastLinkKind=="horizontal" && onHorizontalLine(tail, head, yCoord)))) {
                                            sameLink = true;
                                            if (nextOrientation_=="north") {
                                                nextOrientation_="south";
                                                shift_ = "(\\bx,\\ay-100)";
                                                textPos_ = "below";
                                            } else {
                                                nextOrientation_="north";
                                                shift_ = "(\\bx,\\ay+100)";
                                                textPos_ = "above";
                                            }
                                        } else if (tail==lastTail && (lastLinkKind=="vertical" && onVerticalLine(tail, head, xCoord))) {
                                            sameLink = true;
                                            if (nextOrientation_=="west") {
                                                nextOrientation_="east";
                                                shift_ = "(\\bx+100,\\ay)";
                                                textPos_ = "above";
                                            } else {
                                                nextOrientation_="west";
                                                shift_ = "(\\bx-100,\\ay)";
                                                textPos_ = "below";
                                            }
                                        } 
                                        if(sameLink) {
                                            oneLine = drawRoundLink(link, lineOptions);
                                            lastLinkKind = "round";
                                        } else if (onHorizontalLine(tail, head, yCoord)) {
                                                oneLine = drawHorizontalLink(link, lineOptions, (xCoord[tail] < xCoord[head]));
                                                lastLinkKind = "horizontal";
                                        } else if (onVerticalLine(tail, head, xCoord)) {
                                                oneLine = drawVerticalLink(link, lineOptions, (yCoord[tail] < yCoord[head]));
                                                lastLinkKind = "vertical";
                                        } else {
                                                oneLine = drawDiagonalLink(link, lineOptions, (xCoord[tail] < xCoord[head]),
                                                        (yCoord[tail] < yCoord[head]));
                                                lastLinkKind = "diagonal";
                                        }
                                } 
				if (!oneLine.empty()) outputFile.writeLine(oneLine);
                                sameLink = false;
                                lastTail = tail;
			}
		
		} 
	}	
	
	
	outputFile.writeLine(" \\end{tikzpicture} \n \n");    
}

void LatexNetOutput::printToLaTeX(const std::string& fileWithNodes, const std::string& texFile, 
		bool addHeader, FPType scale) { 
	FileWriter outputFile(texFile);
	std::cout << "file name: " << texFile << std::endl;
	
	if (addHeader) {
		outputFile.writeLine("\\documentclass[a4paper]{article} \n");
		outputFile.writeLine("\\usepackage{a4wide} \n");
		outputFile.writeLine("\\usepackage{tikz} \n \\usetikzlibrary{calc} \n");
                outputFile.writeLine("\\makeatletter \n \\newcommand{\\gettikzxy}[3]{% \n \\tikz@scan@one@point\\pgfutil@firstofone#1\\relax \n \\edef#2{\\the\\pgf@x}% \n \\edef#3{\\the\\pgf@y}% \n } \n \\makeatother \n ");
		outputFile.writeLine("\\begin{document} \n");
	}
        
        if(eLanes_) {
            printNet(net_, outputFile, fileWithNodes, scale);
            printNet(carNet_, outputFile, fileWithNodes, scale);
        } else {
            printNet(net_, outputFile, fileWithNodes, scale);        
        }
	
	if (addHeader) outputFile.writeLine("\\end{document} \n");
	
};

bool LatexNetOutput::plotLink(StarLink* link) const {
	return link->getFlow() > 0.0;
};

void LatexNetOutput::addTikzNodes(FileWriter& outputFile, const std::vector<FPType>& xCoord, 
				const std::vector<FPType>& yCoord, const std::vector<int>& nodeID){
	int nbNodes = net_->getNbNodes();
        bool isZone = false;
        std::string color = "black";
	for (int i = 0; i < nbNodes; ++i) {
		if (xCoord[i] != -1 && yCoord[i] != -1) {
                        StarNode *node = net_->beginNode(net_->getNodeIndex(nodeID[i]));
                        isZone = node->getIsZone();
			std::stringstream tmpss;
                        if (isZone) {
                                color = "green";
			}
                        tmpss << "\\node[draw,thick,circle,"<< color <<",minimum size=0.75cm] (n" << nodeID[i] << ") at (" << xCoord[i] << "," 
                              << yCoord[i] << ") {" << getNodeLabel(nodeID[i]) << "}; \n"; //
                        outputFile.writeLine(tmpss.str());
                        color = "black";
		}
	}
};

std::string LatexNetOutput::getNodeLabel(int nodeID) const {
	std::stringstream ss;
	ss << nodeID;
	return ss.str();
};

//
bool LatexNetOutput::onHorizontalLine(int tail, int head, const std::vector<FPType>& yCoord){
	return (fabs(yCoord[tail] - yCoord[head]) <= COORD_TOL); 
};

bool LatexNetOutput::onVerticalLine(int tail, int head, const std::vector<FPType>& xCoord){
	return (fabs(xCoord[tail] - xCoord[head]) <= COORD_TOL); 
};

std::string LatexNetOutput::getLineLabel(StarLink* link, const std::string& aboveOrBelow){
	std::stringstream ss;
	ss << "-- node[sloped," + aboveOrBelow + "] {" <<  
			link->getFlow() /*link->getTime()*/ << "}";
	return ss.str();
};

std::string LatexNetOutput::drawRoundLink(StarLink* link, const std::string& lineOptions) {
        std::stringstream ss;
        ss << "\\gettikzxy{(n" << link->getNodeFrom() << ")}{\\ax}{\\ay} \n";
        ss << "\\gettikzxy{(n" << link->getNodeTo() << ")}{\\bx}{\\by} \n";

        ss << "\\draw[" << lineOptions << "] (n" << link->getNodeFrom() << "." << nextOrientation_
                << ") .. controls "<< shift_ <<" .. node[sloped," << textPos_ << "]{"<< link->getFlow() /*link->getTime()*/ << "}(n" << link->getNodeTo() << "." << nextOrientation_ << "); \n";
        return ss.str();
};

std::string LatexNetOutput::drawFwdGenLink(StarLink* link, const std::string& lineOptions) {
        std::stringstream ss;
        ss << "\\gettikzxy{(n" << link->getNodeFrom() << ")}{\\ax}{\\ay} \n";
        ss << "\\gettikzxy{(n" << link->getNodeTo() << ")}{\\bx}{\\by} \n";
        ss << "\\draw[dashed," << lineOptions << "] (n" << link->getNodeFrom() << "." <<"north"
                << ") .. controls (\\bx,\\ay+100) .. node[sloped,above]{"<< link->getFlow() /*link->getTime()*/ << "}(n" << link->getNodeTo() << ".north); \n";
        return ss.str();
};

std::string LatexNetOutput::drawHorizontalLink(StarLink* link, const std::string& lineOptions, 
		bool tailIsLeft){
	
	int tailShiftVal = getShift(weInfo_, tailIsLeft);
	std::string tailShift = createShiftStr(weInfo_.shift, tailShiftVal);	
	std::stringstream ss;
	std::string aboveOrBelow("below");
	if (tailIsLeft) aboveOrBelow = "above";
	ss << "\\draw[" << lineOptions <<"] ([" << tailShift << "]n" << link->getNodeFrom() 
					<<	"." << weInfo_.side1 << ") " << getLineLabel(link, aboveOrBelow) << 
					" ([" << tailShift << "]n" << 
					link->getNodeTo() << "." << weInfo_.side2 << "); \n";
	return ss.str();
};

 std::string LatexNetOutput::drawVerticalLink(StarLink* link, const std::string& lineOptions, 
 		bool tailIsUp){
 	int headShiftVal = getShift(snInfo_, tailIsUp);
 	std::string headShift = createShiftStr(snInfo_.shift, headShiftVal);
 	std::stringstream ss;
 	ss << "\\draw[" << lineOptions <<"] ([" << headShift << "]n" << link->getNodeFrom() 
					<<	"." << snInfo_.side1 << ") " << getLineLabel(link, "above") << " ([" << headShift << "]n" << 
					link->getNodeTo() << "." << snInfo_.side2 << "); \n";
	return ss.str();
 };

 std::string LatexNetOutput::drawDiagonalLink(StarLink* link, const std::string& lineOptions, 
 		bool tailIsLeft, bool tailIsUp){
 	int tailShiftVal = getShift(weInfo_, tailIsLeft);
	std::string tailShift = createShiftStr(weInfo_.shift, tailShiftVal);
	int headShiftVal = getShift(snInfo_, tailIsUp);
 	std::string headShift = createShiftStr(snInfo_.shift, headShiftVal);
 	std::stringstream ss;
 	std::string aboveOrBelow("below");
	if (tailIsLeft) aboveOrBelow = "above";
 	ss << "\\draw[" << lineOptions <<"] ([" << headShift << "," << tailShift << "]n" << 
 					link->getNodeFrom() 
					<<	"." << snInfo_.side1 << ") " << getLineLabel(link, aboveOrBelow) 
					<<  " ([" << headShift << "," << 
						tailShift << "]n" << 
					link->getNodeTo() << "." << snInfo_.side2 << "); \n";
 	return ss.str();
 };

 FlowInfo LatexNetOutput::getFlowInfo(StarNetwork* net){
	FPType totalFlow = 0.0;
	FPType minFlow = std::numeric_limits<FPType>::max( );
	FPType maxFlow = 0.0;
	for (StarLink* link = net->beginOnlyLink(); link != NULL; link = net->getNextOnlyLink()) {
		FPType flow = link->getFlow();
		if (flow < minFlow) {
			minFlow = flow;
		}
		if (flow > maxFlow) {
			maxFlow = flow;
		}
		totalFlow += flow;
	}
	FlowInfo flowInfo;
	flowInfo.minFlow = minFlow;
	flowInfo.maxFlow = maxFlow;
	flowInfo.totalFlow = totalFlow;
	return flowInfo;
};

// sets line options for output: blue in general, red if flow/capacity>1, green if fwdgenlink
std::string LatexNetOutput::generateLineOptions(const FlowInfo& flowInfo, StarLink* link){
	std::stringstream sstm;
	FPType tmpFlow = (link->getFlow() - flowInfo.minFlow) / (flowInfo.maxFlow - flowInfo.minFlow);
	FPType capacity = (link->getLinkFnc())->getCapacity();
	std::string color("blue");
	if (link->isFwdGenLink()) {
                color ="green";
        } else if(eLanes_ && link->isELane()) {
                color="orange";
        } else if (link->getFlow() / capacity > 1) {
		color = "red";
	}
	double lineWidth = 0.5 + 5 * tmpFlow - 0.5 * tmpFlow;
	sstm << "line width=" << lineWidth << ", " << color << ", ->";
	return sstm.str();
};

int LatexNetOutput::getShift(PlotInfo& node, bool swapValues){
	int shiftVal = 5;
	if (swapValues) {
		std::swap(node.side1, node.side2);
		return -shiftVal;
	}
	return shiftVal;
}

std::string LatexNetOutput::createShiftStr(const std::string& shift, int val){
	std::stringstream ss;
	ss << shift << "=" << val << "pt";
	return ss.str();
};