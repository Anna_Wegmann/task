/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   NetData.h
 * Author: anna
 *
 * Created on October 18, 2016, 11:34 AM
 */

#ifndef NETDATA_H
#define NETDATA_H


#include "Error.h"
#include "UsedTypes.h"
#include <iostream>
#include <cassert>
class LinkData {
public:
        LinkData(int destID, FPType capacity, FPType length, FPType freeFlowTime, FPType b, FPType power, FPType speed, FPType toll, int type, bool eLane): 
    destID_(destID), capacity_(capacity), length_(length), freeFlowTime_(freeFlowTime), b_(b), power_(power), speed_(speed), toll_(toll), type_(type), eLane_(eLane) {};
    
        int getdestID() {
                return destID_; 
        };
        
        FPType getCapacity() {
                return capacity_;
        };

        FPType getLength() {
                return length_;
        };
        
        FPType getFreeFlowTime() {
                return freeFlowTime_;
        };
        
        FPType getB() {
                return b_;
        };
        
        FPType getPower() {
                return power_;
        };
        
        FPType getSpeed() {
                return speed_;
        }
        
        FPType getToll() {
                return toll_;
        }
        
        int getType() {
                return type_;
        }
        
        bool isELane() {
                return eLane_;
        }
        
private:
        int destID_;
        FPType capacity_;
        FPType length_;
        FPType freeFlowTime_;
        FPType b_;
        FPType power_;
        FPType speed_;
        FPType toll_;
        int type_;
        bool eLane_;
};

class FwdGenLinkData { // fncType_=-1 if no demand function.. means no elastic demand....
public:
    FwdGenLinkData(int destID, FPType demand, int fncType, FPType alpha, FPType beta) : destID_(destID), demand_(demand), fncType_(fncType), alpha_(alpha), beta_(beta) {};
    FwdGenLinkData(int destID, int fncType, FPType alpha, FPType beta) : destID_(destID), demand_(0), fncType_(fncType), alpha_(alpha), beta_(beta) {};
    FwdGenLinkData(int destID, FPType demand) : destID_(destID), demand_(demand), fncType_(-1) {};
    
    void updateDemand(FPType demand) {
        demand_ = demand;
    };
    int getDestID() {
        return destID_;
    };
    
    FPType getDemand() {
            return demand_;
    }
    
    int getFncType() {
            return fncType_;
    }
    
    FPType getAlpha() {
            return alpha_;
    }
    
    FPType getBeta() {
            return beta_;
    }
private:
        int destID_;
        FPType demand_;
        int fncType_;
        FPType alpha_;
        FPType beta_;
};

class NodeData {
public:
    NodeData( int nodeID, bool isZone) : nodeID_(nodeID), isZone_(isZone), curFwdGenLinkData_(-1) {
            std::vector<LinkData*> tmp;
            linkData_ = tmp;

    };
    
    void addLinkData(LinkData* linkData) {
        linkData_.push_back(linkData);
    }
    
    void addFwdGenLinkData(FwdGenLinkData* fwdGenLinkData) {
        fwdGenLinkData_.push_back(fwdGenLinkData);
    }
    
    int getNodeID() {
        return nodeID_;
    }
    
    void updateDemand(int destID, FPType demand) {
        for (unsigned int i=0; i<fwdGenLinkData_.size();i++) {
            if (fwdGenLinkData_[i]->getDestID()==destID) {
                fwdGenLinkData_[i]->updateDemand(demand);
                return;
            }
        }
        std::cout << " new OD-Pair Data stored. This should NOT happen when using ELASTIC DEMAND..." << std::endl;
        fwdGenLinkData_.push_back(new FwdGenLinkData(destID, demand)); // if no elastic demand, the demand is stored in dummy fwdGenLinkData... which can not calc a demand function
//        throw Error("there exists no forward generating demand function for this non zero demand...");
    }
    
    bool isZone() {
        return isZone_;
    }
    
    FwdGenLinkData* getFwdGenLinkData(int fwdGenLinkIndex) {
        return fwdGenLinkData_[fwdGenLinkIndex];
    }
    
    int getFwdGenLinkDataSize() {
        return fwdGenLinkData_.size();
    }
    
    unsigned int getLinkDataSize() {
        return linkData_.size();
    }
    
    LinkData* getLinkData(int linkDataIndex) {
        return linkData_[linkDataIndex];
    }
    
        FwdGenLinkData* beginFwdGenLinkData() {
                curFwdGenLinkData_=0;
                if (getFwdGenLinkDataSize()>0) {
                        return fwdGenLinkData_[curFwdGenLinkData_];
                } else {
                        return NULL;
                }
        }
        
        FwdGenLinkData* getNextFwdGenLinkData(){
                curFwdGenLinkData_++;
                if (curFwdGenLinkData_<getFwdGenLinkDataSize()) {
                        return fwdGenLinkData_[curFwdGenLinkData_];
                } else {
                        return NULL;
                }
        }
    
private:
        int nodeID_;
        bool isZone_;
        int curFwdGenLinkData_;
        std::vector<LinkData*> linkData_;
        std::vector<FwdGenLinkData*> fwdGenLinkData_;
};

class NetData {
public:
    NetData(std::string netName,int nbNodes,int nbLinks,int nbZones, bool eLanes) : netName_(netName), nbNodes_(nbNodes), nbLinks_(nbLinks), nbZones_(nbZones), eLanes_(eLanes), curNodeIndex_(-1){       
    }
    
    void addNodeData(NodeData* nodeData) {
        nodes_.push_back(nodeData);
    }
    
    void addNbLinks(int nbLinks) {
        nbLinks_+=nbLinks;
    }
    
    int getIndex(int nodeID) {
            for (unsigned int i=0; i<nodes_.size(); i++) {
                    if (nodes_[i]->getNodeID() == nodeID) {
                        return i;
                    }
            }
            return -1;        
    }
    
    void addLinkData(int originID, LinkData* linkData) {
            int originIndex = getIndex(originID);
            bool isZone=false;
            if (originIndex==-1) {
                    if (nodes_.size() < getNbZones()) {
                            isZone = true;
                    }
                    addNodeData(new NodeData(originID, isZone));
                    originIndex = getIndex(originID);
            }   
            nodes_[originIndex]->addLinkData(linkData);
    }
    
    void addFwdGenLinkData(int originID, FwdGenLinkData* fwdGenLinkData) { // only called for real fwdGenLinks..
        int originIndex = getIndex(originID);
        if (originIndex==-1 || (unsigned)originIndex>getNbZones()) {
            throw Error("something went wrong in NetData::addFwdGenLinkData...");
        }
        nodes_[originIndex]->addFwdGenLinkData(fwdGenLinkData);
        nbLinks_++;
    }
    
    void updateDemand(int originID, int destID, FPType demand) {
        int originIndex = getIndex(originID);
        nodes_[originIndex]->updateDemand(destID, demand);
    }
    
    std::string getNetName() {
        return netName_;
    }
    
    int getNbNodes() {
        return nbNodes_;
    }
    
    unsigned int getNbZones() {
        return nbZones_;
    }
    
    NodeData* getNodeData (int nodeIndex) {
        return nodes_[nodeIndex];
    }
    
    int getNbLinks() { //nb Links NOT including forward generating links
        return nbLinks_;
    }
    
    int getNbLinks(bool carNetwork) {
            int nbLinks = 0;
            if (!eLanes_ || !carNetwork ) { return nbLinks_; }
            for (unsigned int i=0; i<nodes_.size();i++) {
                    for (unsigned int j=0; j<nodes_[i]->getLinkDataSize();j++) {
                            if (!eLanes_ || !carNetwork || !(nodes_[i]->getLinkData(j)->isELane())) {
                                    nbLinks++;
                            }
                    } 
                    // do not add FwdGenLinkDataSize if not elasticDemand... maybe: add another variable to netData...
                    if(nodes_[i]->getFwdGenLinkDataSize()!=0 && nodes_[i]->getFwdGenLinkData(0)->getFncType()!=-1) {
                            nbLinks += nodes_[i]->getFwdGenLinkDataSize();                    
                    }
            }
            //std::cout << "this network has a calculated number of " << nbLinks << " links" << std::endl;
            return nbLinks;
    }
    
    int getNbOrigins() {
            int nbOrigins = 0;
            for (unsigned int i=0;i<nodes_.size();i++) {
                    if (nodes_[i]->getFwdGenLinkDataSize()!=0) {
                            nbOrigins++;
                    }
            }
            return nbOrigins;
    }
    
    NodeData* beginNode() {
            curNodeIndex_ = 0;
            return nodes_[curNodeIndex_];
    }
    
    NodeData* getNextNode() {
            curNodeIndex_++;
            if (curNodeIndex_<getNbNodes()) {
                    return nodes_[curNodeIndex_];                
            } else {
                    return NULL;
            }
    }
    
    bool isENetwork() {
            return eLanes_;
    }
    
    void setCarToELinkIndices(std::vector<int> carToELinkIndices) {
            carToELinkIndices_ = carToELinkIndices;
    }
    
    int getELinkIndex(int carLinkIndex) {
            assert(carLinkIndex < (int)carToELinkIndices_.size());
            return carToELinkIndices_[carLinkIndex];
    }
    
    int getCarLinkIndex(int eLinkIndex) {
            for (unsigned int j=0; j< carToELinkIndices_.size(); j++) {
                    if (carToELinkIndices_[j]==eLinkIndex) {
                            return j;
                    }
            }
            return -1;
    }
    
    int getNbCarLinks() {
            return carToELinkIndices_.size();
    }
    
    
private:
        std::string netName_;
        int nbNodes_;
        int nbLinks_;
        unsigned int nbZones_;
        bool eLanes_;
        int curNodeIndex_;
        std::vector<NodeData*> nodes_;
        std::vector<int> carToELinkIndices_;
};









#endif /* NETDATA_H */

