#include "Origin.h"
#include "PairOD.h"
#include "Error.h"

#include <cassert>
#include <stdlib.h>
#include <iostream>

Origin::Origin(int index) : index_(index) {
};

Origin::~Origin(){
	for (PairODIterator jt = begin(); jt != end(); ++jt) {
		delete *jt;
	}
};

void Origin::addDestination(PairOD *dest){
	assert(dest != NULL);
	destList_.push_front(dest);
};

bool Origin::updateDemand(PairOD *newDemandPair) {
        int destIndex = newDemandPair->getIndex();
        int odIndex;
        if (destList_.empty()) {
                std::cout << "Something went wrong..." << std::endl;
        }
        for (PairODIterator it = begin(); it!=end();it++ ) {
                if ((*it)->getIndex()==destIndex) {
                        odIndex = (*it)->getODIndex();
                        newDemandPair->setODIndex(odIndex);
                        // DEBUG
                        std::cout<< "Inserting new demand pair ..." << std::endl;
                        newDemandPair->print();
                        destList_.insert(it, newDemandPair); // change it here???
                        if (newDemandPair->getDemand()!=(*it)->getDemand()) {
                                std::cout << "demand changed for odIndex " << odIndex  << " from " << (*it)->getDemand() << " to " << newDemandPair->getDemand() << std::endl;
                        }
                        destList_.erase(it);
                        return true;
                }
        }
        std::cout << "Tried to change demand for link that goes from: " << getIndex() << " to: " << destIndex 
                  << " but was not found in OD-Matrix. It was supposed to be set to " << newDemandPair->getDemand() << std::endl;
        std::cout << "Upper demand bound set to 0." << std::endl;
        return false;
        /*throw Error("PairOD does not exist... Demand can not be updated."); */
};
	
PairODIterator Origin::begin(){
	return destList_.begin();
};	

PairODIterator Origin::end(){
	return destList_.end();
};

int Origin::getIndex() const{
	return index_;
};

bool Origin::isEmpty() const{
	return destList_.empty();
};

int Origin::getNbDest() const{
	return destList_.size();
};

void Origin::remove(PairODIterator it) {
        destList_.erase(it);
};
