/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DefaultDemandParser.cpp
 * Author: anna
 * 
 * Created on October 18, 2016, 3:05 PM
 */

#include "DefaultDemandParser.h"
#include "FileReader.h"
#include "UtilsString.h"
#include "Error.h"
#include "StarNetwork.h"

#include <sstream>
#include <stdlib.h>

/** Internal utility structure.
*/ 
struct ParamsLine{
	int origin;
	int dest;
	int functionType;
	FPType alpha;
	FPType beta;
};

DefaultDemandParser::DefaultDemandParser(const std::string& fileWithNetwork) : fileWithNetwork_(fileWithNetwork) {
}

DefaultDemandParser::~DefaultDemandParser() {
}


void DefaultDemandParser::parseDemand(NetData* netData) {
        FileReader readFile(fileWithNetwork_);
        
        // skip metadata
	std::string line = "";
	while (line.find("<END OF METADATA>") == std::string::npos) {
		line = readFile.getNextLine(); 
		
		if (!readFile.isGood()){ 
			std::string message = "<END OF METADATA> is missing in file: ";
			message.append(fileWithNetwork_);
			Error er(message);
			throw er;
		}
	}
        
        
        ParamsLine params;
        char semic;
        
        while (readFile.isGood()) { 
                line = readFile.getNextLine(); 
                line = Utils::skipOneLineComment("~", line);

                if (!Utils::deleteWhiteSpaces(line).empty()){
                        std::stringstream strLine(line);
                        strLine >> params.origin >> params.dest >> params.functionType >> params.alpha >> 
                                                params.beta >> semic;

                        netData->addFwdGenLinkData(params.origin, new FwdGenLinkData(params.dest, params.functionType, params.alpha, params.beta));
                }        
        }
}
