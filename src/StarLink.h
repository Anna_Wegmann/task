#ifndef STAR_LINK
#define STAR_LINK

#include "LinkFnc.h"
#include "DemandFnc.h"

/** \brief This class is a basic element of StarNetwork. It represents a link of the graph.
*/
class StarLink {
	public:
		/** @param nodeFrom node ID where link comes from.
			@param nodeTo node ID where link points to.
			@param fnc link cost function.
		*/
		StarLink(int nodeFrom, int nodeTo, LinkFnc *fnc, bool isELane=false);
                
                /**
                 * does the same as StarLink(nodeFrom, nodeTo, fnc) for foward generating link, sets demandFnc, forwardGeneratinglInk=true
                 */
                StarLink(int nodeFromID, int nodeToID, LinkFnc *costFnc, DemandFnc *demandFnc);
		virtual ~StarLink();
		
                /**
                 * loads baseLoad for this Link used for carNetwork / eCarNetwork und updates time
                 * @param baseLoad
                 */
                void preload(FPType baseLoad) {
                        baseLoad_ = baseLoad;
                        updateTime();
                }
                
		/** @return ID (ID is obtained from a file with network data) of a node
				where this link comes from.
		*/
		int getNodeFrom() const;
		
		/** @return ID (ID is obtained from a file with network data) of a node
				where this link leads to.
		*/
		int getNodeTo() const;
		
		/** @return current travel time of this link.
			\note It is not automatically updated when flow changes
				(motivation for this is the fact that some of the algorithms now better
				when to update travel time).
		*/
		virtual FPType getTime() const;

		/** @return this pointer unless overridden by derived classes.
		*/
		virtual StarLink* getForwardLink();

		/** @return pointer to a link cost function - in some cases the derivative of the function 
			must be evaluated without physically changing link flow.
		*/
		LinkFnc* getLinkFnc() const;
		
		/** @return internal index (NOT ID) of a node where this link comes from.
		*/
		int getNodeFromIndex() const;
		/** @return internal index (NOT ID) of a node where this link leads to.
		*/
		int getNodeToIndex() const;
		
		/** Sets internal node index of a node where this link comes from.
		*/
		void setNodeToIndex(int index);
		/** Sets internal node index of a node where this link leads to.
		*/
		void setNodeFromIndex(int index);
		
		/** @return current link flow.
		*/
		FPType getFlow() const;
		/** Sets current link flow.
		*/
		void setFlow(FPType flow);
		/** Sets link flow to zero.
		*/
		void setFlowToZero();
		
		/** Adds a specified amount of flow \b flow to current link flow.
		*/
		void addFlow(FPType flow);
		/** @return internal link index.
		*/
		int getIndex() const;
		/** Sets internal link index.
		*/
		void setIndex(int index);
		
		/** Updates current link travel time value and the value of link cost function
			derivative.
		*/
		void updateTime();

		/** @return string representation of link.
		*/
		std::string toString() const;

		/** @return current link cost function derivative value.
		*/
		FPType getDerivative() const;
                
                void setBeta(FPType beta);
                void initDemandFnc(FPType maxDemand);
                
                void setUpperBound(FPType upperBound);

                DemandFnc* getDemandFnc();
                
                bool isFwdGenLink() {
                    return fwdGenLink_;
                }
                
                /**
                 * deletes the function Pointer. Called in updateFwdGenLinkDestID, before removing the Link....
                 */
                
                void deleteFncPointer() {
                    fnc_ = NULL;
                    demandFnc_=NULL;
                }
                
                void setDemandFnc(DemandFnc * demandFnc) {
                    demandFnc_=demandFnc;
                }
                
                bool isELane () {
                        return eLane_;
                }
	private:
		int index_;
		const int nodeFromID_;
		const int nodeToID_;
		int nodeFromIndex_;
		int nodeToIndex_;
		FPType flow_;
                FPType baseLoad_;
		FPType time_;
		LinkFnc *fnc_;
                DemandFnc *demandFnc_;
		FPType der_;
                bool fwdGenLink_;
                bool eLane_;
                FPType upperDemBound_;
};
#endif
