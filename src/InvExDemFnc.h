/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ExcessDemandFnc.h
 * Author: anna
 *
 * Created on September 23, 2016, 11:00 AM
 */

#ifndef INVEXDEMFNC_H
#define INVEXDEMFNC_H

#include "LinkFnc.h"

class InvExDemFnc : public LinkFnc {
    
public:
    
        /**
         * constructs inverse demand function, fncType according to demandFunction...
         * @param fncType
         * @param alpha
         * @param beta
         * @param orgDemand
         */
        InvExDemFnc(int fncType, FPType alpha, FPType beta, FPType orgDemand);
        
        ~InvExDemFnc(); // destructor?!

        void checkExcessDemand(FPType excessDemand) const;
        
        FPType evaluate(FPType linkFlow) const;
        FPType evaluateDerivative(FPType linkFlow) const;
        FPType evaluateSecondDerivative(FPType linkFlow) const;

        void print() const;

        virtual std::string toString() const { //TODO add to constants.h
            return "Excess Demand fnc";
        }

        LinkFnc* clone() const {
            return new InvExDemFnc(*this);
        }

        FPType evaluateTimeAndDerivative(FPType linkFlow, FPType &der);

        virtual FncType getType();

        void setUpperDemandBound(FPType upperBound);

        FPType getUpperDemandBound();
    
private:
        int fncType_;
        const FPType alpha_;
        const FPType beta_;
        const FPType orgDemand_;
        FPType upperDemandBound_;

        bool maxDemandSet() const;

};

#endif /* EXCESSDEMANDFNC_H */

