/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DemandFnc.cpp
 * Author: anna
 * 
 * Created on October 3, 2016, 5:00 PM
 */

#include "DemandFnc.h"
#include <iostream>
#include <math.h>
#include "Error.h"


DemandFnc::DemandFnc(int fncType, FPType alpha, FPType beta, FPType orgDemand): fncType_(fncType), alpha_(alpha), beta_(beta), orgDemand_(orgDemand), upperDemBound_(orgDemand)  {
        std::cout << "Demand function of type " << fncType << " created" << std::endl; 
        
        
}

DemandFnc::~DemandFnc() {
}

FPType DemandFnc::evaluate(FPType cost){
        if (fncType_==0) {
                /** exponential demand function ... ***/
                return beta_*orgDemand_*exp(-alpha_*cost);    
        } else if (fncType_==1) {
                /** linear demand function ... ***/  // 2 node/3Node example...
                if (orgDemand_==0) {
                        return 0;
                } else {
                        return beta_-alpha_*cost;
                        // return beta_-alpha_*cost;
                }                
        } else {
                throw Error("Can not find demand function type");
        }
}

void DemandFnc::setUpperBound(FPType upperBound) {
        upperDemBound_=upperBound;
}
